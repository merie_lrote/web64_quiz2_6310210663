const bcrypt = require('bcrypt');
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET 
const mysql = require('mysql');

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'RoomBookingSystem'
})

connection.connect();
const express = require('express')
const app = express()
const port = 4000

/* Midleware for Authenticating User Token */
function authenticateToken(req, res, next){
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if(token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if (err){ return res.sendStatus(403)
        }else{
             req.user = user
             next()
        }
    })
}

/* API for Regisrering a new room */

app.post("/register_guest", authenticateToken, (req, res) => {

   let name = req.query.name
   let surname = req.query.surname
   let username = req.query.username
   let password = req.query.password

    bcrypt.hash(password, SALT_ROUNDS, (err, hash)=> {
        let query = `INSERT INTO Guest
                    (GuestName, GuestSurname, Username, Password, IsAdmin) 
                    VALUES ('${name}', '${surname}','${username}',
                            '${hash}', false)`
        console.log(query)
        console.log(err)

        connection.query(query, (err, rows) => {
        if (err) {
            res.json({
                "status" : "400",
                "massage" : "Error inserting data into db"
            })
        }else {
            res.json({
                "status" : "200",
                "massage" : "Adding room succesfuul"
            })
        }
        });

    })
})

app.post("/login", (req,res) => {
    let username =  req.query.username
    let user_password =  req.query.password
    let query = `SELECT * FROM Guest WHERE Username='${username}'`
    connection.query( query,(err, rows)=> {
        if (err){
            console.log(err)
            res.json({
                        "status" : "400", 
                        "message" : "Error querying from running db"
                    })
        }else{
            let db_password = rows[0].Password
            bcrypt.compare(user_password, db_password, (err, result)=>{
                if(result){
                    let payload = {
                        "username" : rows[0].Username,
                        "user_id" : rows[0].GuestID,
                        "IsAdmin" : rows[0].IsAdmin,
                    }
                    console.log(payload)
                    let token = jwt.sign(payload, TOKEN_SECRET, { expiresIn : '1d'})
                    res.json(token)
                }else{
                    res.send("Invalid username / password")
                }
            })
        }
    })
})

app.get("/list_room", (req, res) => {
query = "SELECT * from Room";
connection.query(query, (err, rows) => {
    if (err) {
        res.json({
            "status" : "400",
            "massage" : "Error querying from running db"
        })
    }else {
        res.json(rows)
    }
})
})

app.post("/add_room", (req, res) => {

    let room_name =  req.query.room_name
    let room_price = req.query.room_price

    let query = `INSERT INTO Room
                (RoomName, RoomPrice) 
                VALUES ('${room_name}', '${room_price}')`
    console.log(query)


    connection.query(query, (err, rows) => {
        if (err) {
            res.json({
                "status" : "400",
                "massage" : "Error inserting data into db"
            })
        }else {
            res.json({
                "status" : "200",
                "massage" : "Adding room succesfuul"
            })
        }
    });
})

app.post("/update_room", (req, res) => {

    let room_name =  req.query.room_name
    let room_price = req.query.room_price
    let room_id =  req.query.room_id

    let query = `UPDATE Room SET
                RoomName='${room_name}',
                RoomPrice='${room_price}'
                WHERE RoomID=${room_id}`
    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            res.json({
                "status" : "400",
                "massage" : "Error updating recode"
            })
        }else {
            res.json({
                "status" : "200",
                "massage" : "Updating room succesfuul"
            })
        }
    });
})

app.post("/delete_room", (req, res) => {

    let room_id =  req.query.room_id

    let query = `DELETE FROM Room WHERE RoomID=${room_id}`
    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            res.json({
                "status" : "400",
                "massage" : "Error deleting recode"
            })
        }else {
            res.json({
                "status" : "200",
                "massage" : "Deleting room succesfuul"
            })
        }
    });
})
app.listen(port, ()=>{
    console.log(` Now starting Runinng System Backend at ${port} `)
})